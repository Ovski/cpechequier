package echiquier.tests;
import echiquier.outils.DeplacementEtat;
import echiquier.outils.Position;
import echiquier.plateau.Echiquier;

/**
 * Classe de test
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			Echiquier eq = new Echiquier();
			eq.afficheTableau();
			//Cavalier
			assert eq.getDeplacementEtat(new Position(1,0), new Position(0,2)) == DeplacementEtat.SUCCES;
			assert eq.getDeplacementEtat(new Position(1,0), new Position(3,1)) == DeplacementEtat.DEJAOCCUPE;
			assert eq.getDeplacementEtat(new Position(1,0), new Position(3,2)) == DeplacementEtat.INCORRECT;
			assert eq.getDeplacementEtat(new Position(1,0), new Position(13,2)) == DeplacementEtat.HORSDUPLATEAU;
			//Tour
			assert eq.getDeplacementEtat(new Position(0,0), new Position(0,2)) == DeplacementEtat.SUCCES;
			assert eq.getDeplacementEtat(new Position(0,0), new Position(3,0)) == DeplacementEtat.DEJAOCCUPE;
			assert eq.getDeplacementEtat(new Position(0,0), new Position(3,1)) == DeplacementEtat.INCORRECT;
			assert eq.getDeplacementEtat(new Position(0,0), new Position(0,6)) == DeplacementEtat.SUCCES;
			assert eq.getDeplacementEtat(new Position(0,0), new Position(0,7)) == DeplacementEtat.COLLISION;
			//Fou
			assert eq.getDeplacementEtat(new Position(2,0), new Position(4,2)) == DeplacementEtat.SUCCES;
			assert eq.getDeplacementEtat(new Position(2,0), new Position(4,3)) == DeplacementEtat.INCORRECT;
			//Roi
			assert eq.getDeplacementEtat(new Position(3,7), new Position(3,6)) == DeplacementEtat.DEJAOCCUPE;
			//Roi
			assert eq.getDeplacementEtat(new Position(4,7), new Position(4,0)) == DeplacementEtat.COLLISION;
	}
}
