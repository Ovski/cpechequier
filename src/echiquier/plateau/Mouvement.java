package echiquier.plateau;

import echiquier.outils.Position;

public class Mouvement {

	Position pInt;
	Position pFin;
	
	public Mouvement(Position pi, Position pf) {
		pInt = pi;
		pFin = pf;
	}

	public Position getPI() {
		return pInt;
	}
	
	public Position getPF() {
		return pFin;
	}
}
