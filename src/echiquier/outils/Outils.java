package echiquier.outils;

import echiquier.pieces.Piece;

/**
 * Classe de méthodes static pour alléger la classe Echiquier
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Outils {

    /**
     * Vérifie si une position est dans l'échiquier
     *
     * @param Position p: Position à tester
     * @return boolean: Vrai si la position est sur l'échiquier
     */ 
	public static boolean isInTheTableau(Position position) {
		if ( 
			 position.getX() >= 0
			 && position.getX() <=7
			 && position.getY() >=0
			 && position.getY() <=7
		   )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    /**
     * Vérifie si un déplacement est diagonal
     *
     * @param Position pi: Position initiale
     * @param Position pf: Position finale
     * @return boolean: Vrai si le déplacement est diagonal
     */ 
	public static boolean isDeplacementDiagonal(Position pi, Position pf) {
		int deltaX = pf.getX() - pi.getX();
		int deltaY = pf.getY() - pi.getY();
		return Math.abs(deltaY) == 1 && Math.abs(deltaX) == 1;
	}

    /**
     * Decode les états de déplacement
     *
     * @param DeplacementEtat etat
     * @return String: Erreur décodée de manière compréhensible
     */ 
	public static String decodeErreur(DeplacementEtat etat) {
		switch (etat) {
		case INCORRECT:
			return "Deplacement incorrect";
		case HORSDUPLATEAU:
			return "Deplacement hors du plateau";
		case SUCCES:
			return "Deplacement effectué";
		case COLLISION:
			return "Une piece bloque le passage";
		case DEJAOCCUPE:
			return "Une piece alliée est déjà sur cette position";
		case PASDEPIECE:
			return "Vous n'avez pas pris de pièce";
		case ECHECAUROI:
			return "Vous crééz un echec au roi, L2P";
		default:
			return "ERROR GROS BUG A REGLER";
		}
	}

    /**
     * Recupère la couleur opposée à la pièce
     *
     * @param Piece piece
     * @return Couleur
     */ 
	public static Couleur getCouleurOppose(Piece piece) {
		if (piece.getCouleur() == Couleur.BLANC) {
			return Couleur.NOIR;
		}
		return Couleur.BLANC;
	}
}
