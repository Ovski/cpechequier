package echiquier.outils;

/**
 * Enumeration de couleurs
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public enum Couleur {
	BLANC,
	NOIR;
	
	@Override
	public String toString() {
		switch(this) {
			case BLANC: return "Blanc";
			case NOIR: return "Noir";
			default: return "Couleur inconnue";
		}
	}
}