package echiquier.outils;

/**
 * Enumeration des différents états (erreurs ou succes) pour les essais de deplacement
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public enum DeplacementEtat {
	HORSDUPLATEAU,
	INCORRECT,
	DEJAOCCUPE,
	COLLISION,
	SUCCES,
	PASDEPIECE,
	ECHECAUROI,
	ECHECETMAT,
	MANGE
}
