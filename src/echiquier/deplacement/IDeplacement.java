package echiquier.deplacement;


import echiquier.outils.Couleur;
import echiquier.outils.Position;
/**
 * Méthodes à définir pour les objets se déplaçant (les pièces)
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public interface IDeplacement {

    /**
     * Vérifie si un déplacement est correcte ou non
     *
     * @param Position pi: Position initiale de la pièce
     * @param Position pf: Position finale de la pièce
     * @param Couleur c: Couleur de la pièce
     * @return boolean: Vrai si le déplacement est correct
     */ 
	public boolean deplacemementCorrect(Position pi, Position pf, Couleur c);

    /**
     * Renvoi la position suivant la position initiale en fonction de la position finale d'une pièce
     *
     * @param Position pi: Position initiale de la pièce
     * @param Position pf: Position finale de la pièce
     * @param Couleur c: Couleur de la pièce
     * @return Position: la position suivante
     */ 
	public Position next(Position pi, Position pf, Couleur c);
}
