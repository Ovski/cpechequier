package echiquier.deplacement;

import echiquier.outils.Couleur;
import echiquier.outils.Position;

/**
 * Gestion des déplacements des cavaliers
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class DCavalier implements IDeplacement {

	@Override
	public boolean deplacemementCorrect(Position pi, Position pf, Couleur c) {
		int deltaX = pi.getX() - pf.getX();
		int deltaY = pi.getY() - pf.getY();
		
		if(Math.abs(deltaX) == 1 && Math.abs(deltaY) == 2
				|| Math.abs(deltaX) == 2 && Math.abs(deltaY) == 1)
			return true;
		
		return false;
	}

    @Override
	public Position next(Position pi, Position pf, Couleur c)
	{
		return pf;
	}

}
