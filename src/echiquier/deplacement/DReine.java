package echiquier.deplacement;

import echiquier.outils.Couleur;
import echiquier.outils.Position;

/**
 * Gestion des déplacements des reines
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class DReine implements IDeplacement {

	@Override
	public boolean deplacemementCorrect(Position pi, Position pf, Couleur c) {
		if ((new DFou().deplacemementCorrect(pi, pf, c)) || new DTour().deplacemementCorrect(pi, pf, c))
			return true;
		else
			return false;
	}

    @Override
	public Position next(Position pi, Position pf, Couleur c)
	{
		if ((new DFou().deplacemementCorrect(pi, pf, c)))
			return new DFou().next(pi, pf, c);
		else
			return new DTour().next(pi, pf, c);
	}

}
