package echiquier.deplacement;

import echiquier.outils.Couleur;
import echiquier.outils.Position;

/**
 * Gestion des déplacements des pions
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class DPion implements IDeplacement {

	@Override
	public boolean deplacemementCorrect(Position pi, Position pf, Couleur c) {
		int deltaX = pf.getX() - pi.getX();
		int deltaY = pf.getY() - pi.getY();

		if (c == Couleur.BLANC) {
			// Test d'avancement 1 case, ou test de capture diagonale, ou test
			// de premier mouvement
			if (deltaY == 1 && deltaX == 0 
					|| deltaY == 1 && Math.abs(deltaX) == 1 
					|| pi.getY() == 1 && deltaY == 2 && deltaX == 0 )
				return true;
		} else {
			if (deltaY == -1 && deltaX == 0 
					|| deltaY == -1 && Math.abs(deltaX) == 1 
					|| pi.getY() == 6 && deltaY == -2 && deltaX == 0)
				return true;
		}

		return false;
	}

    @Override
	public Position next(Position pi, Position pf, Couleur c) {
    	int deltaY = pf.getY() - pi.getY(); 
		if (Math.abs(deltaY) == 2) {
			return new Position(pi.getX(), pi.getY() + deltaY/2);			
		}
		return pf;
	}
}
