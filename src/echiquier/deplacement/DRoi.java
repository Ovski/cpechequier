package echiquier.deplacement;

import echiquier.outils.Couleur;
import echiquier.outils.Position;

/**
 * Gestion des déplacements des rois
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class DRoi implements IDeplacement {

	@Override
	public boolean deplacemementCorrect(Position pi, Position pf, Couleur c) {
		if (
				new DReine().deplacemementCorrect(pi, pf, c)
				&& Math.abs(pi.getX()-pf.getX()) <=1
				&& Math.abs(pi.getY()-pf.getY())<=1
		   )
			return true;
		else
			return false;
	}

    @Override
	public Position next(Position pi, Position pf, Couleur c)
	{
		return pf;
	}

}
