package echiquier.deplacement;



import echiquier.outils.Couleur;
import echiquier.outils.Position;

/**
 * Gestion des déplacements des fous
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class DFou implements IDeplacement {

	@Override
	public boolean deplacemementCorrect(Position pi, Position pf, Couleur c) {
		if (Math.abs(pf.getX()-pi.getX())== Math.abs(pf.getY()-pi.getY()))
			return true;
		else
			return false;
	}

    @Override
	public Position next(Position pi, Position pf, Couleur c)
	{
		int xNext = pi.getX(), yNext = pi.getY();

		if(pi.getX() < pf.getX())
			xNext = pi.getX()+1;
		else if (pi.getX() > pf.getX())
			xNext = pi.getX()-1;

		if(pi.getY() < pf.getY())
			yNext = pi.getY()+1;
		else if (pi.getY() > pf.getY())
			yNext = pi.getY()-1;
		else
			yNext = pi.getY();
		
		return new Position(xNext, yNext);
	}
}
