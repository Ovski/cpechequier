package echiquier.deplacement;

import echiquier.outils.Position;

import echiquier.outils.Couleur;

/**
 * Gestion des déplacements des tours
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class DTour implements IDeplacement {

	@Override
	public boolean deplacemementCorrect(Position pi, Position pf, Couleur c) {
		if(pi.getX() == pf.getX() || pi.getY() == pf.getY())
			return true;
		else
			return false;
	}

    @Override
	public Position next(Position pi, Position pf, Couleur c)
	{
		int xNext = pi.getX(), yNext = pi.getY();
		if(pi.getY() == pf.getY()) {
			if(pi.getX() < pf.getX())
				xNext = pi.getX()+1;
			else if (pi.getX() > pf.getX())
				xNext = pi.getX()-1;
			else
				xNext = pi.getX();
		}
		if(pi.getX() == pf.getX()) {
			if(pi.getY() < pf.getY())
				yNext = pi.getY()+1;
			else if (pi.getY() > pf.getY())
				yNext = pi.getY()-1;
			else
				yNext = pi.getY();
		}
		return new Position(xNext, yNext);
	}

}
