package echiquier.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	ServerSocket s;
	Socket c;

	public Server(String addr, int port) {
		try {
			s = new ServerSocket(4321);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void getClient() {

		try {
			c = s.accept();
		} catch (IOException e) {
			System.out.println("Accept failed: 4321");
			System.exit(-1);
		}
	}

	public void getData() {
		BufferedReader in;
		PrintWriter out;

		try {
			in = new BufferedReader(new InputStreamReader(c.getInputStream()));
			out = new PrintWriter(c.getOutputStream(), true);

			while (true) {
				try {
					String line = in.readLine();
					// Send data back to client
					out.println(line);
				} catch (IOException e) {
					System.out.println("Read failed");
					System.exit(-1);
				}
			}

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

}
