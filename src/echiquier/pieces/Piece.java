package echiquier.pieces;

import echiquier.deplacement.IDeplacement;
import echiquier.outils.*;

/**
 * Classe représentant une pièce
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public abstract class Piece {
	protected Couleur couleur;
	protected IDeplacement deplacement;

	public Piece(Couleur couleur, IDeplacement deplacement) {
		this.couleur = couleur;
		this.deplacement = deplacement;
	}

	public Couleur getCouleur() {
		return couleur;
	}

	public void setCouleur(Couleur couleur) {
		this.couleur = couleur;
	}

	public IDeplacement getDeplacement() {
		return deplacement;
	}
	
	public void setDeplacement(IDeplacement deplacement) {
		this.deplacement = deplacement;
	}

	/**
     * Vérifie si un déplacement est correcte ou non
     *
     * @param Position pi: Position initiale de la pièce
     * @param Position pf: Position finale de la pièce
     * @param Couleur c: Couleur de la pièce
     * @return boolean: Vrai si le déplacement est correct
     */ 
	public boolean deplacementCorrect(Position pi, Position pf, Couleur c) {
		return this.deplacement.deplacemementCorrect(pi, pf, c);
	}

    /**
     * Renvoi la position suivant la position initiale en fonction de la position finale d'une pièce
     *
     * @param Position pi: Position initiale de la pièce
     * @param Position pf: Position finale de la pièce
     * @param Couleur c: Couleur de la pièce
     * @return Position: la position suivante
     */
	public Position next(Position pi, Position pf, Couleur c) {
		return this.deplacement.next(pi, pf, c);
	}
}
