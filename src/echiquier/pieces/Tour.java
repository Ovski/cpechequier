package echiquier.pieces;

import echiquier.deplacement.DTour;
import echiquier.outils.Couleur;

/**
 * Classe représentant une Tour
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Tour extends Piece {

	public Tour(Couleur couleur) {
		super(couleur, new DTour());
	}
}