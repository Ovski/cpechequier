package echiquier.pieces;

import echiquier.deplacement.DFou;
import echiquier.outils.Couleur;

/**
 * Classe représentant un fou
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Fou extends Piece {

	public Fou(Couleur couleur) {
		super(couleur, new DFou());
	}
}
