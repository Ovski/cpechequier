package echiquier.pieces;

import echiquier.deplacement.DCavalier;
import echiquier.outils.Couleur;

/**
 * Classe représentant un cavalier
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Cavalier extends Piece {

	public Cavalier(Couleur couleur) {
		super(couleur, new DCavalier());
	}
}
