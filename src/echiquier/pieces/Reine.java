package echiquier.pieces;

import echiquier.deplacement.DReine;
import echiquier.outils.Couleur;

/**
 * Classe représentant une reine
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Reine extends Piece{

	public Reine(Couleur couleur) {
		super(couleur, new DReine());
	}
}
