package echiquier.pieces;

import echiquier.deplacement.DPion;
import echiquier.outils.Couleur;

/**
 * Classe représentant un pion
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Pion extends Piece {

	public Pion(Couleur couleur) {
		super(couleur, new DPion());
	}
}
