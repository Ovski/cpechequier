package echiquier.pieces;

import echiquier.deplacement.DRoi;
import echiquier.outils.Couleur;

/**
 * Classe représentant un roi
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Roi extends Piece {

	public Roi(Couleur couleur) {
		super(couleur, new DRoi());
	}
}
