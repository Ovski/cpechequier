package echiquier.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import echiquier.outils.Couleur;
import echiquier.outils.DeplacementEtat;
import echiquier.outils.Outils;
import echiquier.outils.Position;
import echiquier.pieces.Piece;
import echiquier.plateau.Echiquier;

/**
 * Gestion de la fenêtre du jeu d'échec
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class Fenetre extends JFrame implements MouseListener,
		MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	JLayeredPane layeredPane;
	JPanel chessBoard;
	JLabel chessPiece;
	JLabel deplacement;
	int xAdjustment;
	int yAdjustment;

	Couleur cote;

	Couleur tour;

	Echiquier eq;

	Dimension boardSize;

	Position PI;
	Position PF;

	JPanel panelI;

	public Fenetre(final Echiquier eq, Couleur side) {
		super();

		this.eq = eq;

		cote = side;

		boardSize = new Dimension(600, 600);

		layeredPane = new JLayeredPane();
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(layeredPane, BorderLayout.CENTER);
		JPanel southPanel = new JPanel();
		deplacement = new JLabel("Déplacement : ");
		southPanel.add(deplacement);

		getContentPane().add(southPanel, BorderLayout.SOUTH);
		layeredPane.setPreferredSize(boardSize);
		layeredPane.addMouseListener(this);
		layeredPane.addMouseMotionListener(this);

		// Add a chess board to the Layered Pane

		chessBoard = new JPanel();
		layeredPane.add(chessBoard, JLayeredPane.DEFAULT_LAYER);
		chessBoard.setLayout(new GridLayout(8, 8));
		chessBoard.setPreferredSize(boardSize);
		chessBoard.setBounds(0, 0, boardSize.width, boardSize.height);

	}

	public Couleur getTour() {
		return tour;
	}

	public void setTour(Couleur tour) {
		this.tour = tour;
	}

	public void drawBackground() {
		for (int i = 0; i < 64; i++) {
			JPanel square = new JPanel(new BorderLayout());
			chessBoard.add(square);

			int row = ((i / 8) + (cote == Couleur.NOIR ? 1 : 0)) % 2;
			if (row == 0)
				square.setBackground(i % 2 == 0 ? Color.LIGHT_GRAY
						: Color.WHITE);
			else
				square.setBackground(i % 2 == 0 ? Color.WHITE
						: Color.LIGHT_GRAY);
		}
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		if (chessPiece == null)
			return;
		chessPiece
				.setLocation(me.getX() + xAdjustment, me.getY() + yAdjustment);
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		chessPiece = null;
		Component c = chessBoard.findComponentAt(e.getX(), e.getY());

		if (c instanceof JPanel)
			return;

		PI = getPosition(e.getX(), e.getY());
		if (eq.getPieceAt(PI).getCouleur() != tour)
			return;
		panelI = (JPanel) c.getParent();

		Point parentLocation = c.getParent().getLocation();
		xAdjustment = parentLocation.x - e.getX();
		yAdjustment = parentLocation.y - e.getY();
		chessPiece = (JLabel) c;
		chessPiece.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
		chessPiece.setSize(chessPiece.getWidth(), chessPiece.getHeight());
		layeredPane.add(chessPiece, JLayeredPane.DRAG_LAYER);

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (chessPiece == null)
			return;

		chessPiece.setVisible(false);
		Component c = chessBoard.findComponentAt(e.getX(), e.getY());

		PF = getPosition(e.getX(), e.getY());

		// System.out.println("pi : " + PI + "pf " + PF);

		DeplacementEtat et = eq.getDeplacementEtat(PI, PF);

		if (!PI.equals(PF)) {
			String labelText = eq.getPieceAt(PI).getClass().getSimpleName()
					+ " " + eq.getPieceAt(PI).getCouleur() + " se déplace de ("
					+ PI.getX() + ", " + PI.getY() + ")" + " a  (" + PF.getX()
					+ ", " + PF.getY() + ")";
			if (et != DeplacementEtat.SUCCES) {
				// Erreur de déplacement
				JOptionPane.showMessageDialog(this, Outils.decodeErreur(et));
				panelI.add(chessPiece);
			} else if (eq.deplacer(PI, PF) == DeplacementEtat.ECHECETMAT) {
				JOptionPane.showMessageDialog(this, "Echec et Mat, au revoir");
			} else {
				if (c instanceof JLabel) {
					// Succès du déplacement sur une case pleine
					labelText += " et mange "
							+ eq.getPieceAt(PF).getClass().getSimpleName()
							+ " " + eq.getPieceAt(PF).getCouleur();
					Container parent = c.getParent();
					parent.remove(0);
					parent.add(chessPiece);
				} else {
					// Succès du déplacement sur une case vide
					Container parent = (Container) c;
					parent.add(chessPiece);
				}
				tour = (tour == Couleur.BLANC ? Couleur.NOIR : Couleur.BLANC);
				deplacement.setText(labelText);
				eq.deplacer(PI, PF);
			}
		} else {
			// Retour à la case départ
			Container parent = (Container) c;
			parent.add(chessPiece);
		}

		deplacement.repaint();
		chessPiece.setVisible(true);
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param eq
	 *            Le plateau de jeu.
	 * @param side
	 *            Le côté duquel le joueur joue. false pour blanc, true pour
	 *            noir.
	 */
	void displayEchiquier(Echiquier eq) {

		for (Entry<Position, Piece> e : eq.getPlateau().entrySet()) {

			Piece p = e.getValue();

			addPiece(p, e.getKey().getX(), e.getKey().getY());

		}
	}

	public void removePiece(int x, int y) {
		System.out.println("remove");
		JPanel panel = (JPanel) chessBoard
				.getComponent((cote == Couleur.NOIR ? y : 7 - y) * 8 + x);
		panel.removeAll();
		this.repaint();
	}

	public void addPiece(Piece p, int x, int y) {
		System.out.println("add");
		JPanel panel = (JPanel) chessBoard
				.getComponent((cote == Couleur.NOIR ? y : 7 - y) * 8 + x);
		JLabel piece = getPieceJPanel(p);
		panel.add(piece);
		piece.repaint();
		panel.repaint();
		this.repaint();

	}

	private JLabel getPieceJPanel(Piece p) {
		String SPiece = "";

		switch (p.getClass().getSimpleName()) {
		case "Pion":
			SPiece = p.getCouleur() == Couleur.BLANC ? "♙" : "♟";
			break;
		case "Tour":
			SPiece = p.getCouleur() == Couleur.BLANC ? "♖" : "♜";
			break;
		case "Cavalier":
			SPiece = p.getCouleur() == Couleur.BLANC ? "♘" : "♞";
			break;
		case "Fou":
			SPiece = p.getCouleur() == Couleur.BLANC ? "♗" : "♝";
			break;
		case "Roi":
			SPiece = p.getCouleur() == Couleur.BLANC ? "♔" : "♚";
			break;
		case "Reine":
			SPiece = p.getCouleur() == Couleur.BLANC ? "♕" : "♛";
			break;
		}

		JLabel piece = new JLabel(SPiece, SwingConstants.CENTER);
		Font cFont = new Font(piece.getFont().getName(), piece.getFont()
				.getStyle(), 64);
		piece.setFont(cFont);
		return piece;
	}

	public Position getPosition(float x, float y) {

		int xp, yp;

		xp = (int) Math.floor(x / boardSize.getWidth() * 8);

		if (cote == Couleur.BLANC) {
			// Plateau retourné

			yp = 7 - (int) Math.floor(y / boardSize.getHeight() * 8);
		} else {
			yp = (int) Math.floor(y / boardSize.getHeight() * 8);
		}

		return new Position(xp, yp);
	}
}
