package echiquier.gui;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import echiquier.outils.Couleur;
import echiquier.outils.DeplacementEtat;
import echiquier.outils.Outils;
import echiquier.outils.Position;
import echiquier.plateau.Echiquier;

/**
 * Interface pour le jeu d'échec en ligne de commande
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class CLI {

	private static final String move_regex = "m.*([0-8]).*([0-8]).*([0-8]).*([0-8])";
	private Pattern movePattern;
	private Matcher matcher;
	private Echiquier eq;
	Couleur joueurCourrant = Couleur.BLANC;

	CLI(Echiquier eq) {
		this.eq = eq;
		movePattern = Pattern.compile(move_regex);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Echiquier eq = new Echiquier();
		CLI gui = new CLI(eq);

		eq.initPlateau();

		gui.gui();

	}

	public void gui() {
		System.out.println("Jeu d'échec\n");

		while (true) {
			eq.afficheTableau();
			System.out.println("Au joueur "
					+ (joueurCourrant == Couleur.BLANC ? "blanc" : "noir")
					+ " de jouer\n"
					+ "Entrez une commande (h)");
			Scanner a = new Scanner(System.in);
			String i = a.nextLine();
			switch (i) {
			case "q":
				a.close();
				System.exit(0);
				break;
			case "h":
				System.out.println("You noob");
				break;
			default:
				matcher = movePattern.matcher(i);
				if (matcher.matches()) {
					Position pi = new Position(
							Integer.valueOf(matcher.group(1)),
							Integer.valueOf(matcher.group(2)));
					Position pf = new Position(
							Integer.valueOf(matcher.group(3)),
							Integer.valueOf(matcher.group(4)));
					if (eq.getPieceAt(pi).getCouleur() != joueurCourrant) {
						System.out.println("Cette pièce ne vous appartient pas");
						continue;
					}
					if (eq.getDeplacementEtat(pi, pf) == DeplacementEtat.SUCCES
						) {
						eq.deplacer(pi, pf);
						joueurCourrant = joueurCourrant == Couleur.BLANC ? Couleur.NOIR : Couleur.BLANC;
					} else
						// Ajouter explication, voir getDeplacementEtat()
						System.out.println(Outils.decodeErreur(eq.getDeplacementEtat(
								pi, pf)));
				}

			}
		}
	}
}
