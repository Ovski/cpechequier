package echiquier.gui;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import echiquier.outils.Couleur;
import echiquier.outils.Position;
import echiquier.plateau.Echiquier;

/**
 * Interface pour le jeu d'échec en mode fenêtré (2D)
 * 
 * @author Baptiste BOUCHEREAU <baptiste.bouchereau@cpe.fr>
 * @author Eric GILLET <eric.gillet@cpe.fr>
 */
public class SwingGUI {

	private Echiquier eq;

	public SwingGUI(Echiquier eq) {
		this.eq = eq;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Echiquier eq = new Echiquier();
		SwingGUI gui = new SwingGUI(eq);

		eq.initPlateau();

		gui.gui();
	}

	public void gui() {

		Couleur cote;

		int reply = JOptionPane.showConfirmDialog(null,
				"Jouer en multijoueur ?", "Type de partie",
				JOptionPane.YES_NO_OPTION);

		if (reply == JOptionPane.NO_OPTION) {
			cote = Couleur.BLANC;

		} else {
			JTextField IPField = new JTextField(10);

			JPanel panel = new JPanel(new GridLayout(0, 2));
			panel.add(new JLabel("IP: "));
			panel.add(IPField);
			int result = JOptionPane.showConfirmDialog(null, panel,
					"Data Entry", JOptionPane.OK_CANCEL_OPTION);
			if (result == JOptionPane.CANCEL_OPTION) {
				System.exit(0);
				return;
			} else {
				reply = JOptionPane.showConfirmDialog(null, "Jouer en blanc ?",
						"Couleur", JOptionPane.YES_NO_OPTION);
				if (reply == JOptionPane.YES_OPTION) {
					cote = Couleur.BLANC;
				} else
					cote = Couleur.NOIR;
			}

		}
		final Fenetre fen = new Fenetre(eq, cote);
		fen.drawBackground();
		fen.displayEchiquier(eq);
		fen.setTitle("Echec Critique");
		fen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fen.setVisible(true);
		fen.pack();
		fen.setTour(cote);

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					fen.removePiece(0, 0);
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					fen.addPiece(eq.getPieceAt(new Position(0, 0)), 0, 0);
				}

			}
		}).run();

		fen.repaint();
	}
}
